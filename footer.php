<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */


?>
<div class="footer">
	<div class="container">
		<h3 class="tag-line">30+ years of combined healthcare experience. United for your success.</h3>
		<?php get_sidebar( 'footerfull' ); ?>
	</div>		
</div>
</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

