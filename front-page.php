<?php
get_header(); ?>
<div class="digital-services-wrap">
    <div class="container">
    <div class="row digital-services-front-page">
        <div class="col-md-7 align-self-center">
            <h2>Healthcare’s most experienced SEO and SEM professionals</h2>
            <?php mc_button( 'Explore Our Digital Services', 'digital-services', 'primary'); ?>
        </div>
        <div class="col-md-5 digital-services-img">
            <img src="<?php echo get_template_directory_uri(); ?>/images/digital-services.png"> 
        </div>
    </div>
    <div class="row services-slider" id="services-slider">
        <div class="service-wrap col-12">
            <div class="service-icon">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/google-map-management.png">
            </div>
            <div class="service-box">
                <h4>Google Map Management</h4>
            </div>
        </div>
        <div class="service-wrap col-12">
            <div class="service-icon">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/service-line-seo.png">
            </div>
            <div class="service-box">
                <h4>Service Line SEO</h4>
            </div>
        </div>
        <div class="service-wrap col-12">
            <div class="service-icon">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/analytics-architecture.png">
            </div>
            <div class="service-box">
                <h4>Analytics Architecture</h4>
            </div>
        </div>
        <div class="service-wrap col-12">
            <div class="service-icon">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/website-projects.png">
            </div>
            <div class="service-box">
                <h4>Website Projects</h4>
            </div>
        </div>
        <div class="service-wrap col-12">
            <div class="service-icon">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/google-adwords.png">
            </div>
            <div class="service-box">
                <h4>Google Adwords</h4>
            </div>
        </div>
    </div>
</div>
</div>
<div class="creative-services-front-page">
    <div class="background-color"></div>
    <div class="background-img"></div>
    <div class="container">
        <div class="row">
            <h2>proven, results-oriented healthcare marketing experts.</h2>
            <?php mc_button( 'Explore Our Creative Services', 'creative-services', 'secondary' ); ?>
        </div>
        <div class="row creative-service-offerings">
            <div class="col-lg-4">
                <div class="service-icon-wrap">
                    <div class="icon-left"></div>
                    <div class="service-icon align-items-center">
                        <img class=""src="<?php echo get_template_directory_uri(); ?>/images/video.png">
                    </div>
                    <div class="icon-left"></div>
                </div>
                <div class="service-box">
                    <h4>Video</h4>
                </div>
            </div>
            <div class="col-lg-4">
            <div class="service-icon-wrap">
                    <div class="icon-left"></div>
                        <div class="service-icon text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/branding-and-design.png">
                        </div>
                    <div class="icon-left"></div>
                </div>
                <div class="service-box">
                    <h4>Branding and Design</h4>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="service-icon-wrap">
                    <div class="icon-left"></div>
                    <div class="service-icon">
                        <img  src="<?php echo get_template_directory_uri(); ?>/images/social-media.png">
                    </div>
                    <div class="icon-left"></div>
                </div>
                <div class="service-box">
                    <h4>Social Media</h4>
                </div>
            </div>
        </div>  
    </div>
</div><!--.create-services-front-page -->
<div class="explore-services-wrapper">
    <div class="container">
        <div class="row digital-services-front-page">
            <div class="col-md-6 align-self-center">
                <h2>Grow your brand presence and your patient volume.</h2>
                <?php mc_button( 'Explore The Possibilities', 'about', 'primary'); ?>
            </div>
            <div class="col-md-6">
                <img src="<?php echo get_template_directory_uri(); ?>/images/possibilities.png"> 
            </div>
        </div>
    </div>
</div>
<?php
get_footer();