<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://use.typekit.net/ndu4wpu.css">
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar fixed-top navbar-expand-lg navbar-dark">
			<div class="nav-background"></div>
		<?php if ( 'container' == $container ) : ?>
			<div class="container" >
		<?php endif; ?>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

				

				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse justify-content-end',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
	<?php if ( is_front_page() ): ?>
	
	
		<div class="video-header-wrapper ">
		
			<?php //the_custom_header_markup(); ?>
			
			<video autoplay loop id="header-video" controls controlsList="nodownload" muted plays-inline>
  				<source src="<?php echo get_stylesheet_directory_uri(); ?>/videos/myriad-core-home-page.mp4" type="video/mp4">
			</video>
			
			<div id="controls">
				<!-- <button id="playpause" title="play" onclick="togglePlayPause()">pause</button> -->
				<button id="mute" onclick="toggleMute()"><i class="fa fa-volume-off"></i>X</button>
				<button id="fullscreen" onclick="toggleFullScreen()"><i class="fa fa-expand"></i></button>
			</div>
		</div>
<script>
   
  // Grab a handle to the video
  var video = document.getElementById("header-video");
   // Turn off the default controls
   video.controls = false;

   function togglePlayPause() {
   var playpause = document.getElementById("playpause");
   if (video.paused || video.ended) {
      playpause.title = "pause";
      playpause.innerHTML = "pause";
      video.play();
   }
   else {
      playpause.title = "play";
      playpause.innerHTML = "play";
      video.pause();
   }
}

function toggleFullScreen() {
   if (video.requestFullscreen) {
    video.requestFullscreen();
  } else if (video.mozRequestFullScreen) {
    video.mozRequestFullScreen(); // Firefox
  } else if (video.webkitRequestFullscreen) {
    video.webkitRequestFullscreen(); // Chrome and Safari
  }
}

function toggleMute() {
	var muted = document.getElementById("mute");
	if ( video.muted == true ) {
		mute.innerHTML = '<i class="fa fa-volume-off"></i> ';	
	} else {
		mute.innerHTML = '<i class="fa fa-volume-off"></i>X';
	}
   video.muted = !video.muted;
}
</script>

	<?php endif; ?>
