<?php

function mc_button( $title, $link, $type ){

  
    ?>
     <a href="<?php echo $link;?>" class="mc-button mc-button-<?php echo $type; ?>">
    <!-- <div class="mc-button mc-button-<?php echo $type; ?>"> -->
       
            <div class="arrow text-center">
                <img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $type; ?>-arrow.png">
            </div>
            <div class="button-box"> 
                <p><?php echo $title; ?></p>
            </div>
       
    <!-- </div> -->
    </a>
    <?php
}

function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

function mc_custom_images(){
    add_image_size( 'blog', 835, 320, true );
}

add_action( 'init', 'mc_custom_images' );