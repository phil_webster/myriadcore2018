<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package understrap
 */


/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
if ( ! function_exists ( 'understrap_posted_on' ) ) {
	function understrap_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
		}
		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);
		$posted_on = sprintf(
			esc_html_x( '%s', 'post date', 'understrap' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);
		$byline = sprintf(
			esc_html_x( 'by %s', 'post author', 'understrap' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);
		
		// SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
		$category = get_the_category();
		$useCatLink = true;
		// If post has a category assigned.
		if ($category){
			$category_display = '';
			$category_link = '';
			if ( class_exists('WPSEO_Primary_Term') )
			{
				// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
				$wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
				$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
				$term = get_term( $wpseo_primary_term );
				if (is_wp_error($term)) { 
					// Default to first category (not Yoast) if an error is returned
					$category_display = $category[0]->name;
					$category_link = get_category_link( $category[0]->term_id );
				} else { 
					// Yoast Primary category
					$category_display = $term->name;
					$category_link = get_category_link( $term->term_id );
				}
			} 
			else {
				// Default, display the first category in WP's list of assigned categories
				$category_display = $category[0]->name;
				$category_link = get_category_link( $category[0]->term_id );
			}
			// Display category
			if ( !empty($category_display) ){
				$category_html = '';
				if ( $useCatLink == true && !empty($category_link) ){
				$category_html .= '<span class="post-category">';
				$category_html .= '<a href="'.$category_link.'">'.htmlspecialchars($category_display).'</a>';
				$category_html .= '</span>';
				} else {
				$category_html .= '<span class="post-category">'.htmlspecialchars($category_display).'</span>';
				}
			}
			
		}
		$html = '<span class="posted-on"><i class="fa fa-calendar"></i> ' . $posted_on . '</span>';
		if( is_single()){
			$html .=  '<span class="social-sharing">' . do_shortcode('[supsystic-social-sharing id="1"]') . '</span>';
		} else {
			$html .= '<span class="meta-categories">' . $category_html . '</span>';
		}
		echo $html;
	}
}


/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
if ( ! function_exists ( 'understrap_entry_footer' ) ) {
	function understrap_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'understrap' ) );
			//if ( $categories_list && understrap_categorized_blog() ) {
			//	printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'understrap' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			//}
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '<div class="tag-list"><span class="tag">','</span><span class="tag">','</span></div>' );
			if ( $tags_list ) {
				//printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'understrap' ) . '</span>', $tags_list ); // WPCS: XSS OK.
				echo $tags_list;
			}
		}
		// if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		// 	echo '<span class="comments-link">';
		// 	comments_popup_link( esc_html__( 'Leave a comment', 'understrap' ), esc_html__( '1 Comment', 'understrap' ), esc_html__( '% Comments', 'understrap' ) );
		// 	echo '</span>';
		// }
		// edit_post_link(
		// 	sprintf(
		// 		/* translators: %s: Name of current post */
		// 		esc_html__( 'Edit %s', 'understrap' ),
		// 		the_title( '<span class="screen-reader-text">"', '"</span>', false )
		// 	),
		// 	'<span class="edit-link">',
		// 	'</span>'
		// );
	}
}


/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
if ( ! function_exists ( 'understrap_categorized_blog' ) ) {
	function understrap_categorized_blog() {
		if ( false === ( $all_the_cool_cats = get_transient( 'understrap_categories' ) ) ) {
			// Create an array of all the categories that are attached to posts.
			$all_the_cool_cats = get_categories( array(
				'fields'     => 'ids',
				'hide_empty' => 1,
				// We only need to know if there is more than one category.
				'number'     => 2,
			) );
			// Count the number of categories that are attached to the posts.
			$all_the_cool_cats = count( $all_the_cool_cats );
			set_transient( 'understrap_categories', $all_the_cool_cats );
		}
		if ( $all_the_cool_cats > 1 ) {
			// This blog has more than 1 category so components_categorized_blog should return true.
			return true;
		} else {
			// This blog has only 1 category so components_categorized_blog should return false.
			return false;
		}
	}
}


/**
 * Flush out the transients used in understrap_categorized_blog.
 */
add_action( 'edit_category', 'understrap_category_transient_flusher' );
add_action( 'save_post',     'understrap_category_transient_flusher' );

if ( ! function_exists ( 'understrap_category_transient_flusher' ) ) {
	function understrap_category_transient_flusher() {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		// Like, beat it. Dig?
		delete_transient( 'understrap_categories' );
	}
}