<?php
get_header();
?>
<div class="about-header-wrapper">
<div class="about-header align-content-end">
    <!-- <img src="<?php //echo get_template_directory_uri(); ?>/images/about-header.png"> -->
    <div class="container">
        <div class="row">
            <div class="col-12 text-center align-middle header-text-col">
                <h1 class="header-text">Digital + creative healthcare marketing. Reimagined.</h1>
            </div>
        </div>
        <div class="row">
            
        </div>
    </div>
</div>
</div>
<div class="about-body-wrap">
<div class="container about-body">
    <div class="row about-tag-wrap">
        <div class="col-sm-9  text-center about-tag">
            <h2>Two expert resources, one common goal. A healthy bottom line for your healthcare organization.</h2>
            
        </div>
        <div class="col-sm-9 text-center about-team">
                <p>Together, Myriad Core and Route 1A Advertising are a tight-knit team of healthcare experts working cohesively and diligently to enhance your brand presence, improve consumer perception and, most importantly, grow your patient volume.</p>
            </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="dividing-line"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 myriad-about">
            <h3>Myriad Core—SEO and SEM designed specifically for the healthcare industry</h3>
            <p>Good predictions arise from good data and experienced analysts. That’s where Myriad Core comes in. We earned our first analytics certification in 2007 and have been helping customers architect analytics and experiments ever since.</p>
            <p>Online behavior doesn’t always follow intuitive paths. As healthcare’s most experienced SEO and SEM professionals, Myriad Core works to ensure your organization maintains a dominant online presence—providing such integral digital services as Service Line SEO, Search Page Dominance and Google Map Management.</p>
            <p>We also continue to provide website construction and maintenance services to hospitals, hospital networks, physician networks, specialists, online stores and large manufacturers that supply some of the world’s largest medical brands.</p>
        </div>
        <div class="col-md-5 offset-sm-1 route1a-about">
            <h3>Route 1A Advertising—Full-service, results-oriented healthcare marketing</h3>
            <p>With extensive experience in the healthcare industry, Route 1A Advertising has been sought out by local, regional and national clients to produce thought provoking, perception-shifting campaigns to increase consumer awareness and influence change.</p>
            <p>For nearly two decades, we have forged relationships with a multitude of healthcare-specific clients from small community hospitals to niche caregiver facilities to large metropolitan health systems.</p>
            <p>We have developed numerous high-profile, multi-faceted campaigns designed to elevate brand awareness, build consumer loyalty and meet the growing and changing healthcare needs within the communities our clients serve. Our creative services are inclusive of print, digital/web, television/video/radio, outdoor and more.</p>
        </div>    
    </div>
</div>
</div>

<?php
get_footer();