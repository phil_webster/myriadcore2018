<?php
get_header();
?>
<div class="contact">

<section>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="text-uppercase">Get In Touch</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condimentum porttitor cursus. Duis nec nulla turpis. Nulla lacinia laoreet odio, non lacinia nisl malesuada vel. Aenean malesuada fermentum bibendum.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci. Pellentesque ipsum erat, facilisis ut venenatis eu, sodales vel dolor.</p>


                        <div class="row m-t-40">
                            <div class="col-md-6">
                                <address>
			  <strong>Myriad Core LLC.</strong><br>
			  2820 W 23rd St #103<br>
			  Erie, PA 16506<br>
			  <abbr title="Phone">P:</abbr> (877) 858-2797
			</address>
                            </div>
                        </div>




                        <div class="social-icons m-t-30 social-icons-colored">
                            <ul>
                                <li class="social-facebook"><a href="https://www.facebook.com/myriadcore/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-twitter"><a href="https://twitter.com/myriadcore" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li class="social-linkedin"><a href="https://www.linkedin.com/adamyakish" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6">
                            
                        <?php echo do_shortcode( '[ninja_form id=1]', false ); ?>
                           
                    </div>
                    

                </div>
            </div>
        </section>
    </div>
    <div class="google-map-wrapper">
        <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1OAffaOcUQv1uOcHfvlW0QCwkbg1kOzdW" width="100%" height="480"></iframe>
    </div>
 
       

<?php
get_footer();