<?php
get_header();
?>
<div class="creative-services">
    <div class="about-header-wrapper">
        <div class="creative-services-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 header-text-col">
                        <h1 class="header-text">high-impact creative for the healthcare industry.</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="creative-services-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <a href="https://vimeo.com/48304450" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/video-russocolt.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="https://vimeo.com/15547261" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/video-pioneers.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/ahn-brochure.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/ahn-brochure.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/psi-np.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/psi.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="https://vimeo.com/14764767" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/video-healing.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="https://vimeo.com/262028105" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/video-birgani.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="https://vimeo.com/48303963" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/video-serafini.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/arnot-np.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/arnot.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="https://vimeo.com/262266295" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/video-brown.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/bridges-bro.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/bridges-bro.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="https://vimeo.com/10376842" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/video-ballerina.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/lemg-np.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/lemg.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="https://vimeo.com/58888893" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/video-margaret.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/elevator-wrap-cmn.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/elevator-wrap-cmn.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/heart-billboard.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/heart-billboard.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/imagine-billboard.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/imagine-billboard.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/fp-np-grace.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/svgrace.jpg"></a>
            </div>
            <div class="col-sm-4">
                <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/print/sv-np-olivia.jpg" data-lity><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/thumbnails/olivia.jpg"></a>
            </div>
        </div>
    </div>
</div>
</div>
<?php 
get_footer();