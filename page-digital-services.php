<?php
get_header();
?>
<div class="digital-services">
    <div class="about-header-wrapper">
        <div class="digital-services-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 header-text header-text-col">
                        <h1 class="header-text">Healthcare Digital Marketing. Redefined.</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="service-google-map-management">
        <img class="desktop-google-map" src="<?php echo get_template_directory_uri(); ?>/images/digital-services-map.png">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Google Map Management</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
            <a href="<?php echo get_template_directory_uri(); ?>/pdfs/map-case-study.pdf" target="_blank" class="case-study d-inline-flex align-items-center">
                <div class="pdf-icon text-center align-middle align-items-center">
                <img class=""src="<?php echo get_template_directory_uri(); ?>/images/pdf-icon.png">
                </div>
                <div class="align-items-center">
                    <p>Network Combines Eight Practices</p>
                </div>
            </a>
        </div>
    </div>
    <div class="service-service-line-seo">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Service Line SEO</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
            <a href="<?php echo get_template_directory_uri(); ?>/pdfs/seo-case-study.pdf" target="_blank" class="case-study d-inline-flex align-items-center">
                <div class="pdf-icon text-center align-middle align-items-center">
                <img class=""src="<?php echo get_template_directory_uri(); ?>/images/pdf-icon.png">
                </div>
                <div class="align-items-center">
                    <p>Thousands Erased from Google Index</p>
                </div>
            </a>
        </div>
    </div>
    <div class="service-analytics-architecture">
        
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Analytics Architecture</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
            <a href="<?php echo get_template_directory_uri(); ?>/pdfs/analytics-case-study.pdf" target="_blank" class="case-study d-inline-flex align-items-center">
                <div class="pdf-icon text-center align-middle align-items-center">
                <img class=""src="<?php echo get_template_directory_uri(); ?>/images/pdf-icon.png">
                </div>
                <div class="align-items-center">
                    <p>Mammogram Optimization</p>
                </div>
            </a>
        </div>
    </div>
    <div class="service-website-projects">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Website Projects</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
            <a href="<?php echo get_template_directory_uri(); ?>/pdfs/website-projects.pdf" target="_blank" class="case-study d-inline-flex align-items-center">
                <div class="pdf-icon text-center align-middle align-items-center">
                <img class=""src="<?php echo get_template_directory_uri(); ?>/images/pdf-icon.png">
                </div>
                <div class="align-items-center">
                    <p>Speed Optimization Increases Conversions</p>
                </div>
            </a>
        </div>
    </div>
    <div class="service-google-adwords">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Google Adwords</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
            <a href="<?php echo get_template_directory_uri(); ?>/pdfs/adwords-case-study.pdf" target="_blank" class="case-study d-inline-flex align-items-center">
                <div class="pdf-icon text-center align-middle align-items-center">
                <img class=""src="<?php echo get_template_directory_uri(); ?>/images/pdf-icon.png">
                </div>
                <div class="align-items-center">
                    <p>Google Certified Adwords Agency</p>
                </div>
            </a>
        </div>
    </div>
</div>
<?php
get_footer();